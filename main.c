#include <stdio.h>

int main(int argc, char* argv[]) {
	if(argc < 2) {
		printf("count: missing file argument\n");
		printf("Usage: `count file.txt'\n");
		return 1;
	}

	FILE* file = fopen(argv[1], "r");
	if(!file) {
		printf("count: cannot open `%s': No such file\n", argv[1]);
		return 1;
	}

	int lines = 0;
	int c;
	while((c = fgetc(file)) != EOF)
		if(c == '\n')
			++lines;

	printf("%d", lines);
	fclose(file);
	return 0;
}