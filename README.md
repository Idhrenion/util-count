**Count**
====================================================================
Simple C command-line utility that counts the number of _new-lines_ in a text file.

Usage: `count file.txt`